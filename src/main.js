
import Vue from 'vue';
import Vuex from 'vuex'
import InputComponent from './components/InputComponent';
import TextComponent from './components/TextComponent';

window.Vue = Vue;

window.Vue.use(Vuex);
window.Vue.component('inputComponent', InputComponent);
window.Vue.component('textComponent', TextComponent);

Vue.config.productionTip = false;

const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        increment (state) {
            state.count++
        }
    }
})

window.vm = new Vue({
    store,
    el: '#page'
});
